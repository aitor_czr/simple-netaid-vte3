/*
 * simple-netaid-vte3.c
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file.
 */

#include <gtk/gtk.h>
#include <vte/vte.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <sys/inotify.h>

#include <simple-netaid/sbuf.h> 
#include <simple-netaid/netproc.h>
#include <simple-netaid/iproute.h>
   
#define EVENT_BUF_LEN  ( 1024 * ( sizeof (struct inotify_event) + 16 ) )

#define WIN_WIDTH 750
#define WIN_HEIGHT 600

#define MAX_SIZE 1024

static bool hided = 0;
static bool connected = 0;
static u_int32_t m_pid = 0;

static GtkWidget *window = NULL;
static GtkWidget *term1 = NULL;
static GtkWidget *term2 = NULL;
static GtkWidget *term3 = NULL;
static GtkWidget *pane1 = NULL;
static GtkWidget *pane2 = NULL;
static GtkStatusIcon *statusIcon = NULL;

/* Functions defined in userinfo.c */
int userinfo_init(void);
const char *username(void);
const char *userhome(void);

/* Functions defined in set_icon.c */
void set_app_icon(GtkWindow *);
void set_connected_icon(GtkStatusIcon *);
void set_disconnected_icon(GtkStatusIcon *);

void update_status_icon();
void on_status_icon_show(GtkMenuItem*, gpointer);
void on_status_icon_exit(GtkMenuItem*, gpointer);
void on_status_icon_activated(GObject*, gpointer);
void on_status_icon_popup(GtkStatusIcon*, guint, guint32, gpointer);
void destroy (GtkWidget*, gpointer);

/* callbacks */
gboolean menu_state_event(GtkWidget*, GdkEventWindowState*, gpointer);
gboolean delete_event(GtkWidget*, GdkEvent*, gpointer);
gboolean callback_inotify(GIOChannel*, GIOCondition, gpointer);

static const char *netaid_warning_message = "\n\
Another instance of simple-netaid-vte3 \n\
is already running.\n";           

static const char *ubus_warning_message = "\n\
Netaid group is not assigned to the current user \n\
account. To add an existing user account to this \n\
group on your system, use the usermod command:\n\n\
 # usermod -aG netaid <username>\n\n\
replacing <username> with the name of the user \n\
you want to add.\n";  

static
void terminate()
{
   if(m_pid != 0) kill(m_pid, SIGTERM);
}

static
int inotinit(void)
{
   int inotfd, wd;
   
   inotfd = inotify_init1(IN_CLOEXEC);
   if ( inotfd < 0 ) {
      perror( "inotify_init" );
   }

   wd = inotify_add_watch( inotfd, "/tmp/snetaid1000", IN_MODIFY);
   if(wd == -1) {
      perror("inotify_and_watch");
   }
  
   return inotfd;
}

static
void inotify_read(int inotfd)
{
   int length = 0, count = 0;
   char buffer[EVENT_BUF_LEN] = {0};

   length = read( inotfd, buffer, EVENT_BUF_LEN ); 
   if(length < 0) {
      perror("read");
   }

   while ( count < length ) {     
      struct inotify_event *event = ( struct inotify_event * ) &buffer[ count ];
      count += sizeof (struct inotify_event) + event->len;
   }

   update_status_icon();
}

static
void sig_handler(int signum,
                 siginfo_t *info,
                 void *extra)
{
   if(signum == SIGUSR1) {
   
      int rc_int_val = info->si_value.sival_int;
      if(rc_int_val != 0) {
 
         m_pid = (u_int32_t) rc_int_val;         
      }
      
   }
}

static
void child_ready(VteTerminal *terminal,
                 GPid pid,
                 GError *error,
                 gpointer user_data)
{
   if (!terminal) return;
   if (pid == -1) gtk_main_quit();
}

// remove a character from char*
static
char *strdelch(char *str, char ch)
{
    char *current = str;
    char *tail = str;

    while(*tail)
    {
        if(*tail == ch)
        {
            tail++;
        }
        else
        {
            *current++ = *tail++;
        }
    }
    *current = 0;
    return str;
}

static
int util_split(const char *str, char c, char ***array)
{
    int count = 1;
    int token_len = 1;
    int i = 0;
    char *t;

    const char *p = str;
    while (*p != '\0') {
        if (*p == c)
            count++;
        p++;
    }

    *array = (char**) malloc(sizeof(char*) * count);
    if (*array == NULL) {
        return -ENOMEM;        
    }

    p = str;
    while (*p != '\0')
    {
        if (*p == c) {
            (*array)[i] = (char*) malloc( sizeof(char) * token_len );
            if ((*array)[i] == NULL) {
                return -ENOMEM;
            }

            token_len = 0;
            i++;
        }
        p++;
        token_len++;
    }
    
    (*array)[i] = (char*) malloc( sizeof(char) * token_len );
    if ((*array)[i] == NULL) {
        return -ENOMEM;
    }

    i = 0;
    p = str;
    t = ((*array)[i]);
    while (*p != '\0') {
        if (*p != c && *p != '\0') {
            *t = *p;
            t++;
        }
        else {
            *t = '\0';
            i++;
            t = ((*array)[i]);
        }
        p++;
    }

    return count;
}

static void
_paned_change_handle_size_green(GtkWidget *widget)
{
  GtkStyleContext * context = gtk_widget_get_style_context (widget);

  GtkCssProvider * css = gtk_css_provider_new ();
  gtk_css_provider_load_from_data (css,
                                   "paned separator{"
                                   "background-size:3px;"
                                   "background-color:#5a7100;"
                                   "min-height: 6px;"
                                   "}",
                                   -1,NULL);


  gtk_style_context_add_provider (context, GTK_STYLE_PROVIDER(css), 
                      GTK_STYLE_PROVIDER_PRIORITY_USER);
}

static void
_paned_change_handle_size_white(GtkWidget *widget)
{
  GtkStyleContext * context = gtk_widget_get_style_context (widget);

  GtkCssProvider * css = gtk_css_provider_new ();
  gtk_css_provider_load_from_data (css,
                                   "paned separator{"
                                   "background-size:3px;"
                                   "min-height: 4px;"
                                   "}",
                                   -1,NULL);


  gtk_style_context_add_provider (context, GTK_STYLE_PROVIDER(css), 
                      GTK_STYLE_PROVIDER_PRIORITY_USER);
}

static
bool CheckForAnotherInstance(const char *filename)
{

   FILE *f;
   int pid_fd;
   struct flock fl;
   char *mypid = NULL;
   pid_t pid;
	  
   pid_fd = open(filename, O_RDWR|O_CREAT, 0640);
   if(pid_fd < 0) {
	  switch(errno) {
	        
	     case ENOMEM:
	        fprintf(stderr, "Cannot open pidfile %s: %s\n", filename, strerror(errno));
	        break;
	     default:
	        fprintf(stderr, "%s: %s\n", filename, strerror(errno) );
	        exit(EXIT_FAILURE);
	  }
   }
      
   if(asprintf(&mypid, "%ld", (intmax_t) getpid()) == -1)  {  
	  fprintf(stderr, "Unable to write pid: %s\n", strerror(errno));
	  exit(EXIT_FAILURE);
   }
   
   ssize_t sz = 0;
   sz = write(pid_fd, mypid, strlen(mypid));
   if(sz == -1) { 
	  fprintf(stderr, "Unable to write pid: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
   }
   free(mypid);

   fl.l_type = F_WRLCK; /* F_RDLCK, F_WRLCK, F_UNLCK */
   fl.l_whence = SEEK_SET; /* SEEK_SET, SEEK_CUR, SEEK_END */
   fl.l_start = 0; /* Offset from l_whence */
   fl.l_len = 0; /* length, 0 = to EOF */
   fl.l_pid = getpid(); /* our PID */

   // try to create a file lock
   if( fcntl(pid_fd, F_SETLK, &fl) == -1) {  /* F_GETLK, F_SETLK, F_SETLKW */

      // we failed to create a file lock, meaning it's already locked //
      if( errno == EACCES || errno == EAGAIN) {
         return true;
      }
   }

   return false;
}

int main(int argc, char *argv[])
{
   int rc __attribute__((unused)) = 0;
   static int inotfd;
   static GIOChannel *ginotfd;
   
   gchar **envp = NULL;
   gchar **command = NULL;
   GtkWidget *menu, *menuItemShow, *menuItemExit;   

   FILE *pf = NULL;
   char output[256]={0};
   bool found = false;
   char lockfile[128]={0};
   bool is_already_running;

   struct sigaction sa;

   const char *user_name;
   const char *user_home;
  
   if( userinfo_init() )
    {
      printf("Cannot retrieve username and home: %s\n", strerror(errno));
    }
  
   user_name = username();
   user_home = userhome();
   
   strcpy(lockfile, user_home);
   strcat(lockfile, "/.simple-netaid-vte.lock");
   
   is_already_running = CheckForAnotherInstance(lockfile);
   if(is_already_running) {
      GtkWidget *dialog;
      gtk_init(&argc, &argv);
      dialog = gtk_message_dialog_new(NULL,
         GTK_DIALOG_DESTROY_WITH_PARENT,
         GTK_MESSAGE_WARNING,
         GTK_BUTTONS_OK,
         netaid_warning_message);
      gtk_window_set_title(GTK_WINDOW(dialog), "simple-netaid warning");
      gtk_window_set_position (GTK_WINDOW(dialog), GTK_WIN_POS_MOUSE);
      gtk_dialog_run(GTK_DIALOG(dialog));
      gtk_widget_destroy(dialog);
      exit(EXIT_FAILURE);
   }
  
   pf = popen("getent group netaid", "r");
   if(!pf) {
      printf("error opening the pipe %s\n", strerror(errno));
      exit(EXIT_FAILURE);
   }

   memset(output, 0, 256);
   if(fgets(output, 256, pf)) {
      char *aux = NULL;
      output[strcspn(output, "\n")] = '\0';
      aux = rindex(output, ':'); 
      if(strcmp(aux, ":")) {
         if(!strcmp(aux+1, user_name)) {
             found = true;
         } else {
            int c = 0;
            char **arr = NULL;   
            c = util_split(aux+1, ',', &arr);
            arr[c-1][strlen(arr[c-1])-1]='\0';
            for(int i=0; i<c; i++) {
               if(!strcmp(arr[i], user_name)) {
                  found = true;
                  break;
               }
            }
         }
      }
   }
   pclose(pf);

   if(!found) {
      GtkWidget *dialog;
      gtk_init(&argc, &argv);
      dialog = gtk_message_dialog_new(NULL,
         GTK_DIALOG_DESTROY_WITH_PARENT,
         GTK_MESSAGE_WARNING,
         GTK_BUTTONS_OK,
         ubus_warning_message);
      gtk_window_set_title(GTK_WINDOW(dialog), "Unallowed operation warning");
      gtk_window_set_position (GTK_WINDOW(dialog), GTK_WIN_POS_MOUSE);
      gtk_dialog_run(GTK_DIALOG(dialog));
      gtk_widget_destroy(dialog);
   }
   
   memset (&sa, 0, sizeof(sa));
   sa.sa_sigaction = &sig_handler;
   sa.sa_flags = SA_NODEFER | SA_SIGINFO;
   sigemptyset(&sa.sa_mask);
   sigaction(SIGUSR1, &sa, 0);
   sigaction(SIGUSR2, &sa, 0);
   
   atexit(terminate);
      
   gtk_init(&argc, &argv);
   window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
   gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
   gtk_window_set_title(GTK_WINDOW(window), "simple-netaid");
   gtk_container_set_border_width (GTK_CONTAINER (window), 2);
   gtk_widget_set_size_request(window, WIN_WIDTH, WIN_HEIGHT);
   
   set_app_icon(GTK_WINDOW(window));
     
   statusIcon = gtk_status_icon_new();   
   gtk_status_icon_set_has_tooltip(statusIcon, TRUE);

   update_status_icon();
 
   menu = gtk_menu_new();
   menuItemShow = gtk_menu_item_new_with_label("Hide");
   menuItemExit = gtk_menu_item_new_with_label("Exit");
    
   g_signal_connect(G_OBJECT (menuItemShow), "activate", G_CALLBACK (on_status_icon_show), window);
   g_signal_connect(G_OBJECT (menuItemExit), "activate", G_CALLBACK (on_status_icon_exit), NULL);
    
   gtk_menu_shell_append(GTK_MENU_SHELL (menu), menuItemShow);
   gtk_menu_shell_append(GTK_MENU_SHELL (menu), menuItemExit);
   
   gtk_widget_show_all(menu);
    
   g_signal_connect(G_OBJECT (statusIcon), "activate", G_CALLBACK (on_status_icon_activated), window);
   g_signal_connect(G_OBJECT (statusIcon), "popup-menu", G_CALLBACK (on_status_icon_popup), menu);
    
   g_signal_connect(G_OBJECT (window), "window-state-event", G_CALLBACK (menu_state_event), menuItemShow); 
    
   gtk_status_icon_set_visible(statusIcon, TRUE);
   gtk_window_set_skip_taskbar_hint(GTK_WINDOW (window), TRUE);
   gtk_window_deiconify(GTK_WINDOW(window));
   //gtk_window_set_keep_above(GTK_WINDOW(window), TRUE);
    
   g_signal_connect (G_OBJECT (window), "destroy", G_CALLBACK (destroy), NULL);
   g_signal_connect (G_OBJECT (window), "delete_event", G_CALLBACK (destroy), statusIcon);
   
   pane1 = gtk_paned_new(GTK_ORIENTATION_VERTICAL);
   pane2 = gtk_paned_new(GTK_ORIENTATION_VERTICAL);
   
   term1 = vte_terminal_new();
   term2 = vte_terminal_new();
   term3 = vte_terminal_new();
  
   gtk_paned_pack1 (GTK_PANED (pane2), term1, TRUE, TRUE);
   gtk_paned_pack2 (GTK_PANED (pane2), pane1, TRUE, TRUE);
   gtk_widget_set_sensitive (term3, FALSE);   
   gtk_paned_pack1 (GTK_PANED (pane1), term2, TRUE, TRUE);
   gtk_paned_pack2 (GTK_PANED (pane1), term3, TRUE, TRUE);
   gtk_widget_set_sensitive (term2, FALSE);
      
   vte_terminal_set_scroll_on_output(VTE_TERMINAL(term2), TRUE);   
   vte_terminal_set_scroll_on_output(VTE_TERMINAL(term3), TRUE);
   
/*
   // Start a new shell
   envp = g_get_environ();
   command = (gchar *[]){g_strdup(g_environ_getenv(envp, "SHELL")), NULL };
   g_strfreev(envp);
*/ 
   
   /***********************************************************************
      \brief 
      There are two async tail processes below. Before starting the 'tail' 
      process in term3, we sleep 1 sec. In doing so, simple-netaid-cdk
      (the process to be launched in term1) will find only one PID 
      related to 'tail' among the children at the very beginning, because 
      the other one didn't start yet, requesting its 'tty' afterwards via:
       
         'ps -p PID -o tty=' 
      
      This child, i.e. the one running in term2, shows the output of 
      every command run through the Ubus
    
   ************************************************************************/  
   g_shell_parse_argv (
         "sh -c 'sleep 1 && tail -f /tmp/snetaid1000/rt_netlink.log'", 
         NULL,
         &command,
         NULL
   );
   vte_terminal_spawn_async (
         VTE_TERMINAL(term3),
         VTE_PTY_DEFAULT,
         NULL,
         command,
         NULL,
         0,
         NULL, NULL, NULL,
         -1,
         NULL, NULL, NULL
   );
                  
 
   g_shell_parse_argv (
         "/usr/bin/tail -f",
         NULL,
         &command,
         NULL
   );
   vte_terminal_spawn_async (
         VTE_TERMINAL(term2),
         VTE_PTY_DEFAULT,
         NULL,
         command,
         NULL,
         0,
         NULL, NULL, NULL,
         -1,
         NULL, NULL, NULL
   );
    
   if( access("/usr/bin/simple-netaid-cdk", F_OK ) == 0 ) {
      g_shell_parse_argv("/usr/bin/simple-netaid-cdk", NULL, &command, NULL);
   } else if( access("/usr/local/bin/simple-netaid-cdk", F_OK ) == 0 ) {  
      g_shell_parse_argv("/usr/local/bin/simple-netaid-cdk", NULL, &command, NULL);
   } else {
      fprintf(stderr, "simple-netaid-cdk not found in your system: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
   }
   vte_terminal_spawn_async (
         VTE_TERMINAL(term1),
         VTE_PTY_DEFAULT,
         NULL,
         command,
         NULL,
         0,
         NULL, NULL, NULL,
         -1,
         NULL,
         child_ready,  // callback
         NULL
   );
   
   /* Connect some signals */
   g_signal_connect(window, "delete-event", gtk_main_quit, NULL);
   g_signal_connect(term1, "child-exited", gtk_main_quit, NULL);

   inotfd = inotinit();
   ginotfd = g_io_channel_unix_new( inotfd );
   g_io_add_watch(ginotfd, G_IO_IN,  callback_inotify, NULL); 

   /* Put widgets together and run the main loop */
   gtk_container_add(GTK_CONTAINER(window), pane1);
   gtk_container_add(GTK_CONTAINER(window), pane2);
   _paned_change_handle_size_white(pane1);
   _paned_change_handle_size_green(pane2);
   gtk_window_resize (GTK_WINDOW(window), WIN_WIDTH, WIN_HEIGHT);
   gtk_widget_show_all(GTK_WIDGET(window));
   if(argc>1 && !strcmp(argv[1], "--systray")) {
      gtk_widget_hide (window);
      hided = true;
   }
     
     gtk_widget_set_size_request (pane2, 300, 50);
   
   gtk_widget_set_size_request(GTK_WIDGET(pane1), 15, 15);
     gtk_widget_set_size_request (GTK_WIDGET(term1), 10, 50);
   gtk_main();
   
   return EXIT_SUCCESS;
}

void update_status_icon()
{
   struct sbuf s; 

   if(*iproute() != 0) {
      connected = TRUE; 
      set_connected_icon(statusIcon);
   } else {
      set_disconnected_icon(statusIcon);
   }
  
   sbuf_init(&s);
   netproc(&s);
   gtk_status_icon_set_tooltip_text(statusIcon, s.buf);
   sbuf_free(&s);
}

void on_status_icon_show(GtkMenuItem *item,
                         gpointer window) 
{
   if(hided) {
      gtk_widget_show (GTK_WIDGET(window));
      hided = false;
   } else {
      gtk_widget_hide (GTK_WIDGET(window));
      hided = true;
   }
}

void on_status_icon_exit(GtkMenuItem *item, 
                         gpointer user_data) 
{
   gtk_main_quit();
}

void on_status_icon_activated(GObject *statusIcon, 
                              gpointer window)
{
   if(hided) {
      gtk_widget_show (GTK_WIDGET(window));
      hided = false;
   } else {
      gtk_widget_hide (GTK_WIDGET(window));
      hided = true;
   }
}

void on_status_icon_popup(GtkStatusIcon *status_icon,
                          guint button, 
                          guint32 activate_time, 
                          gpointer popUpMenu)
{
   gtk_menu_popup(GTK_MENU(popUpMenu), NULL, NULL, 
                  gtk_status_icon_position_menu, 
                  status_icon, 
                  button, 
                  activate_time);
}

void destroy(GtkWidget *window, gpointer data)
{
   gtk_widget_hide (GTK_WIDGET(window));
   hided = true;
}

gboolean delete_event (GtkWidget *window, 
                       GdkEvent *event, 
                       gpointer data)
{
   return FALSE;
}

gboolean menu_state_event(GtkWidget *widget,
                          GdkEventWindowState *event, 
                          gpointer menuItemShow)
{
   if(hided) {
      gtk_menu_item_set_label (menuItemShow, "Show");
   } else {
      gtk_menu_item_set_label (menuItemShow, "Hide");
   }
   return TRUE;
}

/*===================== IOChannel input callback =====================*/
gboolean callback_inotify(GIOChannel *source, 
                          GIOCondition condition,
                          gpointer pData)
{
   int fd = g_io_channel_unix_get_fd(source);
   inotify_read( fd );
   return TRUE;
}
