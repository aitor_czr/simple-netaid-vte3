#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <stdlib.h>
#include <string.h>

static guint32 u8tou32(const guint8* b)
{
  guint32 u;
  int i;
  for(u=*b, i=1; i<4; i++)
    {
      u <<= 8;
      u |= *(b+i);
    }
  return u;
}

/*============================== PIXBUF DATA ================================*/
#include "app_16x16"
#include "connected_16x16"
#include "disconnected_16x16"

/*================= Destructor (dummy because data is static) ===============*/
void nop_destroy (guchar *pixels, gpointer data)
{
  /* NO OP */
}
void set_app_icon(GtkWindow *pWindow)
{
   GdkPixbuf *pPix;
   const char *magic = "GdkP";
   int i;
   guint32 total_len, pixdata_type, width, height, rowstride;
   const guint8 *pixeldata;
  
   if( strncmp(app_16x16, magic, 4) ) {
      fprintf(stderr, "Error in function set_icon(): invalid Gdk pixbuf.\n");
      exit(EXIT_FAILURE);
   }

   total_len = u8tou32(app_16x16+4);
   pixdata_type = u8tou32(app_16x16+8);
   rowstride = u8tou32(app_16x16+12);
   width  = u8tou32(app_16x16+16);
   height = u8tou32(app_16x16+20);
   pixeldata = app_16x16+24;

   pPix=gdk_pixbuf_new_from_data(
      pixeldata, GDK_COLORSPACE_RGB, TRUE, 8,
      width, height, rowstride, nop_destroy, NULL
   );

   gtk_window_set_icon( pWindow, pPix );
}

void set_connected_icon(GtkStatusIcon *pStatusIcon)
{
   GdkPixbuf *pPix;
   const char *magic = "GdkP";
   int i;
   guint32 total_len, pixdata_type, width, height, rowstride;
   const guint8 *pixeldata;
  
   if( strncmp(connected_16x16, magic, 4) ) {
      fprintf(stderr, "Error in function set_icon(): invalid Gdk pixbuf.\n");
      exit(EXIT_FAILURE);
   }

   total_len = u8tou32(connected_16x16+4);
   pixdata_type = u8tou32(connected_16x16+8);
   rowstride = u8tou32(connected_16x16+12);
   width  = u8tou32(connected_16x16+16);
   height = u8tou32(connected_16x16+20);
   pixeldata = connected_16x16+24;

   pPix=gdk_pixbuf_new_from_data(
      pixeldata, GDK_COLORSPACE_RGB, TRUE, 8,
      width, height, rowstride, nop_destroy, NULL
   );

   gtk_status_icon_set_from_pixbuf( pStatusIcon, pPix );
}

void set_disconnected_icon(GtkStatusIcon *pStatusIcon)
{
   GdkPixbuf *pPix;
   const char *magic = "GdkP";
   int i;
   guint32 total_len, pixdata_type, width, height, rowstride;
   const guint8 *pixeldata;
  
   if( strncmp(disconnected_16x16, magic, 4) ) {
      fprintf(stderr, "Error in function set_icon(): invalid Gdk pixbuf.\n");
      exit(EXIT_FAILURE);
   }

   total_len = u8tou32(disconnected_16x16+4);
   pixdata_type = u8tou32(disconnected_16x16+8);
   rowstride = u8tou32(disconnected_16x16+12);
   width  = u8tou32(disconnected_16x16+16);
   height = u8tou32(disconnected_16x16+20);
   pixeldata = disconnected_16x16+24;

   pPix=gdk_pixbuf_new_from_data(
      pixeldata, GDK_COLORSPACE_RGB, TRUE, 8,
      width, height, rowstride, nop_destroy, NULL
   );

   gtk_status_icon_set_from_pixbuf( pStatusIcon, pPix );
}
