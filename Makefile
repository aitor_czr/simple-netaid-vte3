
APP_ICON := simple_netaid.ico
CONNECTED_ICON := network-transmit-receive.ico
DISCONNECTED_ICON := network-wired-disconnected.ico

OBJECTS := simple-netaid-vte3.o set_icon.o userinfo.o
SOURCES := $(patsubst %.o,%.c,$(OBJECTS))

DESTDIR ?=
PREFIX  ?= /usr/local
BINDIR  ?= $(DESTDIR)$(PREFIX)/bin

.SUFFIXES:

all: simple-netaid-vte3

%.o: %.c
	gcc -D_GNU_SOURCE -c $< -Wno-deprecated-declarations `pkg-config --cflags vte-2.91` `pkg-config --cflags gtk+-3.0`

simple-netaid-vte3: $(OBJECTS)
	gcc -o $@ $^ `pkg-config --libs vte-2.91` `pkg-config --libs gtk+-3.0` -lnetaid

set_icon.o: set_icon.c
	gdk-pixbuf-csource --raw --name=app_16x16 $(APP_ICON) >app_16x16
	gdk-pixbuf-csource --raw --name=connected_16x16 $(CONNECTED_ICON) >connected_16x16
	gdk-pixbuf-csource --raw --name=disconnected_16x16 $(DISCONNECTED_ICON) >disconnected_16x16
	gcc -c $< -Wno-deprecated-declarations -I.. `pkg-config --cflags gtk+-3.0`

.SECONDARY: connected_16x16

INSTALLED_FILES = \
	$(BINDIR)/simple-netaid-vte3 \
	$(DESTDIR)$(PREFIX)/share/applications/simple-netaid-vte3.desktop \
	$(DESTDIR)/usr/share/pixmaps/simple_netaid.ico

install: $(INSTALLED_FILES)

$(BINDIR)/simple-netaid-vte3: simple-netaid-vte3
	cp $< $@

$(DESTDIR)$(PREFIX)/share/applications/simple-netaid-vte3.desktop: simple-netaid-vte3.desktop
	mkdir -p $(DESTDIR)$(PREFIX)/share/applications
	cp $< $@

$(DESTDIR)/usr/share/pixmaps/simple_netaid.ico: simple_netaid.ico
	mkdir -p $(DESTDIR)/usr/share/pixmaps
	cp $< $@

uninstall:
	rm -vf $(INSTALLED_FILES)

.PHONY: clean cleanall uninstall

clean:
	@rm -vf $(wildcard *.o) $(wildcard *~)

cleanall:
	@rm -vf $(wildcard *.o) $(wildcard *~) simple-netaid-vte3 app_16x16 connected_16x16 disconnected_16x16

